package com.microservice.rest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/controller")

public class Controller {
	
	@GetMapping("/list")
	public String getList() {
		return "List";
	}

}
